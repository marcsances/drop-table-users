package com.hackeps.orangucards;

import android.app.Service;
import android.content.Intent;
import android.nfc.cardemulation.HostApduService;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

import java.util.logging.Logger;

public class MyHostApduService extends HostApduService {



    @Override
    public byte[] processCommandApdu(byte[] apdu, Bundle extras) {
        Intent intent = new Intent("apducommand");
        intent.putExtra("data",apdu.toString());
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        return apdu;

    }

    public int onStartCommand (Intent intent, int flags, int startId) {
        Intent intent2 = new Intent("apducommand");
        intent2.putExtra("data","service runs");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent2);
        return START_STICKY;
    }

    @Override
    public void onDeactivated(int i) {

    }
}
