package com.hackeps.orangucards;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.nfc.Tag;

public class HelloWorld extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hello_world);
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver, new IntentFilter("apducommand"));
    }

    public void log(String s) {
        TextView tv = this.findViewById(R.id.log_view);
        tv.append(s + "\n");
    }

    public void goClicked(View v) {
        log("hello");
    }
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            log("received");
            String action = intent.getAction();
            String data = intent.getStringExtra("data");
            //  ... react to local broadcast message
            log(data);
        }
    };
}
